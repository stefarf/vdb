#!/usr/bin/env bash
BASE="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
set -e

echo "Create vgtest schema"
(cat <<EOF | docker exec -i mariadb bash) || true
cat <<SQL | mysql -u root -psecret
USE mysql;
CREATE SCHEMA IF NOT EXISTS vdb_test;
SQL
EOF

echo "Run go test"
cd $BASE
go test ./... -coverprofile=cover.out -p 1
go tool cover -func=cover.out >coverage.txt
go tool cover -html=cover.out
rm cover.out
