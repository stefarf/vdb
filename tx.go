package vdb

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/stefarf/iferr"
)

type Tx struct {
	tx        *sqlx.Tx
	slowLimit time.Duration
	slowCb    func(q string, d time.Duration)
}

// RunTransaction begins the transaction and execute the run (callback function).
// At the end:
// - it will commit the transaction if there is no error
// - it will rollback the transaction if there is any error
func (db *DB) RunTransaction(run func(tx *Tx) error) error {
	tx, err := db.Db.Beginx()
	if err != nil {
		return err
	}

	runRecover := func() (err error) {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("panic error: %v", r)
			}
		}()
		return run(&Tx{tx: tx, slowLimit: db.slowLimit, slowCb: db.slowCb})
	}

	err = runRecover()
	if err != nil {
		iferr.Print(tx.Rollback())
		return err
	}
	return tx.Commit()
}

func (db *DB) MustRunTransaction(run func(tx *Tx) error) { iferr.Panic(db.RunTransaction(run)) }

func (tx *Tx) ExecWithResult(q string, args ...any) (sql.Result, error) {
	q, args, err := sqlxInRebind(tx.tx.Rebind, q, args)
	if err != nil {
		return nil, err
	}

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if tx.slowCb != nil && dur > tx.slowLimit {
				tx.slowCb(q, dur)
			}
		}
	}()

	return tx.tx.Exec(q, args...)
}

func (tx *Tx) Exec(q string, args ...any) error {
	_, err := tx.ExecWithResult(q, args...)
	return err
}

func (tx *Tx) MustExec(q string, args ...any) { iferr.Panic(tx.Exec(q, args...)) }

func (tx *Tx) BulkInsert(table string, columns []string, rows [][]any) error {
	_, err := bulkInsert(tx.slowLimit, tx.slowCb, tx.tx, table, columns, rows)
	return err
}

func (tx *Tx) BulkInsertWithResult(table string, columns []string, rows [][]any) (sql.Result, error) {
	return bulkInsert(tx.slowLimit, tx.slowCb, tx.tx, table, columns, rows)
}
