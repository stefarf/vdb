# README

`vdb` is a golang db helper. It is built on top of sqlx.

To run unit test:

- Execute the local db: `./run-mariadb.sh`.
- Execute the test script: `./run-test.sh`.

## Latest Coverage

```text
gitlab.com/stefarf/vdb/bulkinsert.go:14:	bulkInsert		        84.2%
gitlab.com/stefarf/vdb/db.go:15:		    Connect			        75.0%
gitlab.com/stefarf/vdb/db.go:24:		    MustConnect		        0.0%
gitlab.com/stefarf/vdb/db.go:31:		    Open			        0.0%
gitlab.com/stefarf/vdb/db.go:40:		    MustOpen		        0.0%
gitlab.com/stefarf/vdb/db.go:46:		    Close			        100.0%
gitlab.com/stefarf/vdb/db.go:48:		    ExecWithResult		    80.0%
gitlab.com/stefarf/vdb/db.go:57:		    Exec			        100.0%
gitlab.com/stefarf/vdb/db.go:62:		    MustExec		        100.0%
gitlab.com/stefarf/vdb/db.go:64:		    BulkInsert		        100.0%
gitlab.com/stefarf/vdb/db.go:69:		    BulkInsertWithResult	0.0%
gitlab.com/stefarf/vdb/query.go:25:		    query			        80.0%
gitlab.com/stefarf/vdb/query.go:35:		    Query			        100.0%
gitlab.com/stefarf/vdb/query.go:40:		    Query			        0.0%
gitlab.com/stefarf/vdb/query.go:50:		    ScanRow			        68.8%
gitlab.com/stefarf/vdb/query.go:84:		    ScanRows		        79.3%
gitlab.com/stefarf/vdb/query.go:131:		isPtrToStruct		    85.7%
gitlab.com/stefarf/vdb/query.go:143:		isPtr			        100.0%
gitlab.com/stefarf/vdb/tx.go:19:		    RunTransaction		    84.6%
gitlab.com/stefarf/vdb/tx.go:42:		    MustRunTransaction	    0.0%
gitlab.com/stefarf/vdb/tx.go:44:		    ExecWithResult		    80.0%
gitlab.com/stefarf/vdb/tx.go:53:		    Exec			        0.0%
gitlab.com/stefarf/vdb/tx.go:58:		    MustExec		        0.0%
gitlab.com/stefarf/vdb/tx.go:60:		    BulkInsert		        100.0%
gitlab.com/stefarf/vdb/tx.go:65:		    BulkInsertWithResult	0.0%
total:						                (statements)		    71.0%
```