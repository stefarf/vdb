#!/usr/bin/env bash
BASE="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

DATA=$BASE/data/mariadb
mkdir -p $DATA

docker stop mariadb
docker rm mariadb
docker run \
  --detach \
  --name mariadb \
  --env TZ="Asia/Jakarta" \
  --env MARIADB_ROOT_PASSWORD="secret" \
  -v $DATA:/var/lib/mysql \
  -p 3306:3306 \
  mariadb:10.4
