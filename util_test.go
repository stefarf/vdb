package vdb_test

import (
	"testing"
)

func assert(t *testing.T, cond bool, message string) {
	if cond {
		return
	}
	t.Fatal(message)
}

func ifErrFatal(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}
