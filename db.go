package vdb

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/stefarf/iferr"
)

type DB struct {
	Db *sqlx.DB

	slowLimit time.Duration
	slowCb    func(q string, d time.Duration)
}

// Connect connects to a database and verifies with a ping.
func Connect(driverName, dataSourceName string) (*DB, error) {
	db, err := sqlx.Connect(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}
	return &DB{Db: db}, nil
}

// MustConnect is the same as vdb.Connect but panics on error
func MustConnect(driverName, dataSourceName string) *DB {
	db, err := Connect(driverName, dataSourceName)
	iferr.Panic(err)
	return db
}

// Open is the same as sql.Open but returns vdb.DB
func Open(driverName, dataSourceName string) (*DB, error) {
	db, err := sqlx.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}
	return &DB{Db: db}, nil
}

// MustOpen is the same as vdb.Open but panics on error
func MustOpen(driverName, dataSourceName string) *DB {
	db, err := Open(driverName, dataSourceName)
	iferr.Panic(err)
	return db
}

func (db *DB) SetSlowQueryExecCallback(limit time.Duration, cb func(q string, d time.Duration)) *DB {
	db.slowLimit = limit
	db.slowCb = cb
	return db
}

func (db *DB) Ping() error { return db.Db.Ping() }

func (db *DB) Close() error { return db.Db.Close() }

func (db *DB) ExecWithResult(q string, args ...any) (rst sql.Result, err error) {
	q, args, err = sqlxInRebind(db.Db.Rebind, q, args)
	if err != nil {
		return nil, err
	}

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if db.slowCb != nil && dur > db.slowLimit {
				db.slowCb(q, dur)
			}
		}
	}()

	return db.Db.Exec(q, args...)
}

func (db *DB) Exec(q string, args ...any) error {
	_, err := db.ExecWithResult(q, args...)
	return err
}

func (db *DB) MustExec(q string, args ...any) { iferr.Panic(db.Exec(q, args...)) }

func (db *DB) BulkInsert(table string, columns []string, rows [][]any) error {
	_, err := bulkInsert(db.slowLimit, db.slowCb, db.Db, table, columns, rows)
	return err
}

func (db *DB) BulkInsertWithResult(table string, columns []string, rows [][]any) (sql.Result, error) {
	return bulkInsert(db.slowLimit, db.slowCb, db.Db, table, columns, rows)
}
