package vdb_test

import (
	"database/sql"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/stefarf/vdb"
)

type DbTx interface {
	ExecWithResult(q string, args ...any) (sql.Result, error)
}

type Row struct {
	Id       int
	Name     string
	Category string
}

func Test_All(t *testing.T) {
	db, err := vdb.Connect(testDbConfig())
	ifErrFatal(t, err)
	defer db.Close()

	dropRecreateTable := func() {
		ifErrFatal(t, db.Exec("DROP TABLE IF EXISTS products")) // exec with no args
		ifErrFatal(t, db.Exec(`
			CREATE TABLE products (
				id INT NOT NULL AUTO_INCREMENT,
				name VARCHAR(100) NOT NULL,
				category VARCHAR(100) NOT NULL,
				PRIMARY KEY (id)
			)`)) // exec with no args
	}

	t.Run("Drop, recreate table and select row", func(t *testing.T) {
		dropRecreateTable()

		// Select
		var count int
		found, err := db.
			Query("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema=? and table_name='products'",
					dbName).
			ScanRow(&count) // query scan row to non struct ptr
		assert(t, err == nil && found && count == 1, "Must found table products")

		// Select
		type Row struct {
			TableSchema string `db:"table_schema"`
			TableName   string `db:"table_name"`
		}
		var row Row
		found, err = db.Query(`
			SELECT table_schema, table_name FROM information_schema.tables
			WHERE table_schema=? and table_name='products'`, dbName).
			ScanRow(&row) // query scan row to struct ptr
		assert(
			t,
			err == nil && found && reflect.DeepEqual(row, Row{TableSchema: dbName, TableName: "products"}),
			"Must select row containing db and table name")
	})

	insertRows := func(dbTx DbTx) {
		rst, err := dbTx.ExecWithResult(
			"INSERT INTO products (name, category) VALUES (?, ?), (?, ?), (?, ?), (?, ?)",
			"Wireless Mouse", "Electronics",
			"Bluetooth Speaker", "Electronics",
			"Running Shoes", "Footwear",
			"Water Bottle", "Accessories",
		) // exec with result
		ifErrFatal(t, err)
		rowsAffected, err := rst.RowsAffected()
		assert(t, err == nil && rowsAffected == 4, "Must affect 4 rows (inserted)")
	}

	t.Run("Insert, delete and select rows", func(t *testing.T) {
		defer func() {
			if r := recover(); r != nil {
				t.Fatal(r)
			}
		}()

		insertRows(db)

		var ids []int
		ifErrFatal(t, db.Query(`SELECT id FROM products`).ScanRows(&ids)) // query scan rows to ptr of int slice
		assert(t, reflect.DeepEqual(ids, []int{1, 2, 3, 4}), "Must get 4 ids")

		var names []string
		ifErrFatal(t, db.Query(`SELECT name FROM products`).ScanRows(&names)) // query scan rows to ptr of string slice
		assert(t, reflect.DeepEqual(names, []string{
			"Wireless Mouse",
			"Bluetooth Speaker",
			"Running Shoes",
			"Water Bottle",
		}), "Must get 4 names")

		// Select rows
		var rows []Row
		ifErrFatal(t, db.Query(`SELECT * FROM products`).ScanRows(&rows)) // query scan rows to ptr of struct slice
		assert(t, reflect.DeepEqual(rows, []Row{
			{Id: 1, Name: "Wireless Mouse", Category: "Electronics"},
			{Id: 2, Name: "Bluetooth Speaker", Category: "Electronics"},
			{Id: 3, Name: "Running Shoes", Category: "Footwear"},
			{Id: 4, Name: "Water Bottle", Category: "Accessories"},
		}), "Must select exact 4 rows")

		// Delete 2 rows and select remaining rows
		ifErrFatal(t, db.Exec(
			"DELETE FROM products WHERE name IN (?)",
			[]string{"Wireless Mouse", "Running Shoes"})) // exec with []string args
		ifErrFatal(t, db.Query(`SELECT * FROM products`).ScanRows(&rows)) // query scan rows to ptr of struct slice
		assert(t, reflect.DeepEqual(rows, []Row{
			{Id: 2, Name: "Bluetooth Speaker", Category: "Electronics"},
			{Id: 4, Name: "Water Bottle", Category: "Accessories"},
		}), "Must select exact 2 rows")

		// Delete 2 rows and select remaining rows
		db.MustExec(
			"DELETE FROM products WHERE name IN (?)",
			[]string{"Bluetooth Speaker", "Water Bottle"}) // must exec with []string args
		ifErrFatal(t, db.Query(`SELECT * FROM products`).ScanRows(&rows)) // query scan rows to ptr of struct slice
		assert(t, len(rows) == 0, "Must select no rows")
	})

	t.Run("Transaction", func(t *testing.T) {
		ifErrFatal(t, db.Exec("DELETE FROM products"))

		err := db.RunTransaction(func(tx *vdb.Tx) error {
			insertRows(tx)
			return errors.New("expected error")
		})
		assert(t, err.Error() == "expected error", "Must get expected error")

		var count int
		found, err := db.Query(`SELECT COUNT(*) FROM products`).ScanRow(&count) // query scan row to non struct ptr
		ifErrFatal(t, err)
		assert(t, found && count == 0, "Must count 0 products")

		ifErrFatal(t, db.RunTransaction(func(tx *vdb.Tx) error {
			insertRows(tx)
			return nil
		}))
		found, err = db.Query(`SELECT COUNT(*) FROM products`).ScanRow(&count) // query scan row to non struct ptr
		ifErrFatal(t, err)
		assert(t, found && count == 4, "Must count 4 products")
	})

	t.Run("Bulk insert", func(t *testing.T) {
		dropRecreateTable()

		ifErrFatal(t, db.BulkInsert("products", []string{"name", "category"}, [][]any{
			{"Laptop Backpack", "Bags"},
			{"Desk Lamp", "Home & Office"},
			{"Gaming Keyboard", "Electronics"},
		}))

		var rows []Row

		ifErrFatal(t, db.Query(`SELECT * FROM products`).ScanRows(&rows)) // query scan rows to ptr of struct slice
		assert(t, reflect.DeepEqual(rows, []Row{
			{Id: 1, Name: "Laptop Backpack", Category: "Bags"},
			{Id: 2, Name: "Desk Lamp", Category: "Home & Office"},
			{Id: 3, Name: "Gaming Keyboard", Category: "Electronics"},
		}), "Must select exact 3 rows")

		ifErrFatal(t, db.RunTransaction(func(tx *vdb.Tx) error {
			return tx.BulkInsert("products", []string{"name", "category"}, [][]any{
				{"Fitness Tracker", "Electronics"},
				{"Electric Kettle", "Kitchen"},
			})
		}))
		ifErrFatal(t, db.Query(`SELECT * FROM products`).ScanRows(&rows)) // query scan rows to ptr of struct slice
		assert(t, reflect.DeepEqual(rows, []Row{
			{Id: 1, Name: "Laptop Backpack", Category: "Bags"},
			{Id: 2, Name: "Desk Lamp", Category: "Home & Office"},
			{Id: 3, Name: "Gaming Keyboard", Category: "Electronics"},
			{Id: 4, Name: "Fitness Tracker", Category: "Electronics"},
			{Id: 5, Name: "Electric Kettle", Category: "Kitchen"},
		}), "Must select exact 5 rows")
	})

	//t.Run("Cursor scan", func(t *testing.T) {
	//	err := db.Query("SELECT a, b, c FROM products").CursorScan(
	//		func(s vdb.Scanner) error {
	//			var a, b, c string
	//			return s.Scan(&a, &b, &c)
	//		})
	//})
}
