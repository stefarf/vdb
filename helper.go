package vdb

import "github.com/jmoiron/sqlx"

func sqlxInRebind(rebind func(q string) string, q string, args []any) (string, []any, error) {
	q, args, err := sqlx.In(q, args...)
	if err != nil {
		return q, args, err
	}
	q = rebind(q)
	return q, args, nil
}
