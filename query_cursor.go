package vdb

import (
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
)

func (q *Query) CursorScan(cb func(rows *sqlx.Rows) error) (err error) {
	if q.err != nil {
		return q.err
	}
	if q.done {
		return errors.New("can be scanned only once")
	}

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if q.slowCb != nil && dur > q.slowLimit {
				q.slowCb(q.q, dur)
			}
		}
	}()

	q.done = true
	rows, err := q.db.Queryx(q.q, q.args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		err = cb(rows)
		if err != nil {
			return err
		}
	}
	return nil
}
