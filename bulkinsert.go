package vdb

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
)

type DbExec interface {
	Exec(query string, args ...any) (sql.Result, error)
}

func bulkInsert(
	slowLimit time.Duration,
	slowCb func(q string, d time.Duration),
	db DbExec, table string, columns []string, rows [][]any,
) (rst sql.Result, err error) {
	if table == "" {
		panic("no table defined")
	}
	if len(columns) == 0 {
		panic("no columns defined")
	}

	qMarks := "?"
	for i := 2; i <= len(columns); i++ {
		qMarks += ",?"
	}
	qMarks = fmt.Sprintf("(%s)", qMarks)

	anchor := ""
	var args []any
	for _, row := range rows {
		if len(row) != len(columns) {
			return nil, errors.New("row size != number of columns")
		}

		if anchor != "" {
			anchor += ","
		}
		anchor += qMarks
		args = append(args, row...)
	}

	q := fmt.Sprintf("INSERT INTO %s (%s) VALUES %s", table, strings.Join(columns, ","), anchor)

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if slowCb != nil && dur > slowLimit {
				slowCb(q, dur)
			}
		}
	}()

	return db.Exec(q, args...)
}
