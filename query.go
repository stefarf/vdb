package vdb

import (
	"database/sql"
	"errors"
	"reflect"
	"time"

	"github.com/jmoiron/sqlx"
)

type Query struct {
	err  error
	done bool
	db   DbQueryRebind
	q    string
	args []any

	slowLimit time.Duration
	slowCb    func(q string, d time.Duration)
}

type DbQueryRebind interface {
	QueryRowx(query string, args ...interface{}) *sqlx.Row
	Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
	Rebind(query string) string
}

// Query prepare db query
func (db *DB) Query(q string, args ...any) *Query {
	q, args, err := sqlxInRebind(db.Db.Rebind, q, args)
	if err != nil {
		return &Query{err: err}
	}
	return &Query{
		db:        db.Db,
		q:         q,
		args:      args,
		slowLimit: db.slowLimit,
		slowCb:    db.slowCb,
	}
}

// Query prepare db query
func (tx *Tx) Query(q string, args ...any) *Query {
	q, args, err := sqlxInRebind(tx.tx.Rebind, q, args)
	if err != nil {
		return &Query{err: err}
	}
	return &Query{
		db:        tx.tx,
		q:         q,
		args:      args,
		slowLimit: tx.slowLimit,
		slowCb:    tx.slowCb,
	}
}

// ScanRow scans one row and puts the result in the values.
// Values can be:
//   - one value which type is pointer to struct, or
//   - multi values which each type is pointer to something
//
// It returns found=false and err=nil if the query produces no result.
func (q *Query) ScanRow(values ...any) (found bool, err error) {
	if q.err != nil {
		return false, q.err
	}
	if q.done {
		return false, errors.New("can be scanned only once")
	}

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if q.slowCb != nil && dur > q.slowLimit {
				q.slowCb(q.q, dur)
			}
		}
	}()

	q.done = true
	row := q.db.QueryRowx(q.q, q.args...)
	if len(values) == 1 && isPtrToStruct(values[0]) {
		err = row.StructScan(values[0])
	} else {
		for _, v := range values {
			if !isPtr(v) {
				return false, errors.New("scan row values must be pointer")
			}
		}
		err = row.Scan(values...)
	}

	switch {
	case err == nil:
		return true, nil
	case errors.Is(err, sql.ErrNoRows):
		return false, nil
	default:
		return false, err
	}
}

// ScanRows scans many rows and puts the result in the ptrList.
// The ptrList type is pointer to list of rows.
// If the type of row is struct, then the row will be fetched using StructScan, else it will be fetched using Scan.
func (q *Query) ScanRows(ptrLst any) (err error) {
	if q.err != nil {
		return q.err
	}
	if q.done {
		return errors.New("can be scanned only once")
	}

	vPtr := reflect.ValueOf(ptrLst) // *[]Row
	if vPtr.Kind() != reflect.Ptr {
		return errors.New("must be ptr")
	}
	vLst := vPtr.Elem() // []Row
	vNewLst := vLst
	tRow := vLst.Type().Elem() // can be Struct or not Struct

	tStart := time.Now()
	defer func() {
		if err == nil {
			dur := time.Since(tStart)
			if q.slowCb != nil && dur > q.slowLimit {
				q.slowCb(q.q, dur)
			}
		}
	}()

	q.done = true
	rows, err := q.db.Queryx(q.q, q.args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	vNewLst.SetZero()
	for rows.Next() {
		ptrRow := reflect.New(tRow).Interface()

		if tRow.Kind() == reflect.Struct {
			err = rows.StructScan(ptrRow)
			if err != nil {
				return err
			}
		} else {
			err = rows.Scan(ptrRow)
			if err != nil {
				return err
			}
		}

		vItem := reflect.ValueOf(ptrRow).Elem()
		vNewLst = reflect.Append(vNewLst, vItem)
	}
	vLst.Set(vNewLst)
	return nil
}

func isPtrToStruct(val any) bool {
	vPtr := reflect.ValueOf(val)
	if vPtr.Kind() != reflect.Ptr {
		return false
	}
	vStruct := vPtr.Elem()
	if vStruct.Kind() != reflect.Struct {
		return false
	}
	return true
}

func isPtr(val any) bool {
	vPtr := reflect.ValueOf(val)
	return vPtr.Kind() == reflect.Ptr
}
