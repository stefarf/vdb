package vdb_test

import (
	"fmt"
	"os"
	"time"

	"github.com/go-sql-driver/mysql"
)

const (
	dbName = "vdb_test"
)

func testDbConfig() (string, string) {
	dbAddr := fmt.Sprintf("%s:3306", os.Getenv("DB_HOST"))
	dbUser := "root"
	dbPass := "secret"
	dbCfg := mysql.Config{
		Net:                  "tcp",
		Addr:                 dbAddr,
		DBName:               dbName,
		User:                 dbUser,
		Passwd:               dbPass,
		Loc:                  getLocation("Asia/Jakarta"),
		AllowNativePasswords: true,
		MultiStatements:      true,
		ParseTime:            true,
	}
	return "mysql", dbCfg.FormatDSN()
}

func getLocation(name string) *time.Location {
	loc, _ := time.LoadLocation(name)
	return loc
}
